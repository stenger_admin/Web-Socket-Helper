﻿using System;
using System.Diagnostics;

namespace Core.Framework.Util.Common
{
    public class Watch
    {
        /// <summary>
        /// 获取程序运行时间
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static double TimerByMilliseconds(Action action)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            action.Invoke();
            sw.Stop();
            return sw.Elapsed.TotalMilliseconds;
        }
    }
}
