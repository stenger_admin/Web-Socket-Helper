﻿using System.ComponentModel.DataAnnotations;

namespace Core.Framework.Util
{
    public class PhoneNumberAttribute : CustomizeValidationAttribute
    {
        public PhoneNumberAttribute()
        {
            this.ErrorMessage = "手机号码必须11位。";
        }

        public override bool IsValid(object value)
        {
            if (value is string)
            {
                return ((string)value).Length == 11;
            }
            else
            {
                return false;
            }
        }
    }
}
