﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 消息类型
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// 用户登录
        /// </summary>
        UserInfo = 0,

        /// <summary>
        /// 发送消息
        /// </summary>
        SendMessage = 1,

        /// <summary>
        /// 消息广播
        /// </summary>
        Boardcast = 2,

        /// <summary>
        /// 定时消息广播
        /// </summary>
        SetTimeBoardcast = 3,

        /// <summary>
        /// 指定时间发送消息
        /// </summary>
        SetTimeMessage = 4,

        /// <summary>
        /// 修改当前会话KEY
        /// </summary>
        UpdateSession = 5,

        /// <summary>
        /// 系统通知  弃用
        /// </summary>
        System = 6,

        /// <summary>
        /// 创建群组
        /// </summary>
        CreateGroup = 7,

        /// <summary>
        /// 创建讨论组
        /// </summary>
        CreateForum = 8,

        /// <summary>
        /// 修改用户信息
        /// </summary>
        UpdateUserInfo = 9,

        #region New Features

        // 分组管理
        // 拒绝会话 列表
        // 好友列表 { 添加 删除 移动 }{分组|成员}
        // 群组管理 { 解散 退出 加入 剔除 }{创建者|管理员} {群名|自定义扩展信息}
        // 讨论组 { 解散 退出 加入 }

        /// <summary>
        /// 撤回消息
        /// </summary>
        Withdraw = 10,

        /// <summary>
        /// 拒绝会话
        /// </summary>
        Refuse = 11,

        /// <summary>
        /// 好友添加分组
        /// </summary>
        AddFriendGroup = 12,

        /// <summary>
        /// 删除分组
        /// </summary>
        DeleteFriendGroup = 13,

        /// <summary>
        /// 修改分组
        /// </summary>
        UpdateFriendGroup = 14,

        /// <summary>
        /// 添加好友
        /// </summary>
        AddFriend = 15,

        /// <summary>
        /// 删除好友
        /// </summary>
        DeleteFriend = 16,

        /// <summary>
        /// 修改好友自定义信息
        /// </summary>
        UpdateFriendInfo = 17,

        /// <summary>
        /// 移动好友至指定分组
        /// </summary>
        ToFriendGroup = 18,

        /// <summary>
        /// 修改群信息
        /// </summary>
        UpdateGroup = 19,

        /// <summary>
        /// 解散群组
        /// </summary>
        DisbandGroup = 20,

        /// <summary>
        /// 退出群组
        /// </summary>
        DropOutGroup = 21,

        /// <summary>
        /// 申请加入群组
        /// </summary>
        ApplyJoinGroup = 22,

        /// <summary>
        /// 审批加入群组
        /// </summary>
        ReviewGroup = 23,

        /// <summary>
        /// 拒绝加入群组
        /// </summary>
        RefuseGruop = 24,

        /// <summary>
        /// 剔除群组
        /// </summary>
        KickGruop = 25,

        /// <summary>
        /// 授权管理员
        /// </summary>
        AuthorizationGruopAdmin = 26,

        /// <summary>
        /// 取消授权管理员
        /// </summary>
        DeauthorizationGruopAdmin = 27,

        /// <summary>
        /// 解散讨论组
        /// </summary>
        DisbandForum = 29,

        /// <summary>
        /// 退出讨论组
        /// </summary>
        DropOutForum = 30,

        /// <summary>
        /// 修改讨论组
        /// </summary>
        UpdateForum = 31,

        /// <summary>
        /// 剔除讨论组
        /// </summary>
        KickForum = 32,

        /// <summary>
        /// 加入讨论组
        /// </summary>
        JoinForum = 33

        #endregion

    }

}
