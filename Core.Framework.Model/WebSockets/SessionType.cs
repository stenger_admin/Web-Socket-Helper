﻿namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 聊天方式
    /// </summary>
    public enum SessionType
    {
        /// <summary>
        /// 单人对聊
        /// </summary>
        SingleChat = 0,

        /// <summary>
        /// 群组聊天
        /// </summary>
        GroupChat = 1,

        /// <summary>
        /// 语音聊天
        /// </summary>
        VoiceChat = 2,

        /// <summary>
        /// 视频聊天
        /// </summary>
        VideoChat = 3,

        /// <summary>
        /// 创建群组
        /// </summary>
        CreateGroup = 4,

        /// <summary>
        /// 创建讨论组
        /// </summary>
        CreateForum = 5
    }

}
