﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 提交参数
    /// </summary>
    public class QueryMessageTest
    {
        /// <summary>
        /// 主题
        /// </summary>
        public string Template { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 消息Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// 系统Token
        /// </summary>
        public string SystemToken { get; set; }

        /// <summary>
        /// 消息关键字
        /// </summary>
        public string MessageKey { get; set; }

        /// <summary>
        /// 消息类型
        /// </summary>
        public MessageTypeEnum MessageType { get; set; }

        /// <summary>
        /// 消息[属性]参数
        /// </summary>
        public string Parameter { get; set; }

        /// <summary>
        /// 项目token
        /// </summary>
        public string ProjectToken { get; set; }

        /// <summary>
        /// 回调地址
        /// </summary>
        public string CallUrl { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public DateTime SendDateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 重试次数
        /// </summary>
        public int RetryCount { get; set; } = 0;

        /// <summary>
        /// 最大重试次数
        /// </summary>
        public int MaxRetryCount { get; set; } = 2;

        /// <summary>
        /// 延迟时间
        /// </summary>
        public int DelaySeconds { get; set; } = 60;


        

    }
}
