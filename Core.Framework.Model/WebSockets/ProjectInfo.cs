﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.WebSockets
{
    /// <summary>
    /// 项目信息
    /// </summary>
    public class ProjectInfo
    {
        /// <summary>
        /// 项目Token
        /// </summary>
        public string ProjectToken { get; set; }

        /// <summary>
        /// 消息回调地址
        /// </summary>
        public string CallUrl { get; set; }
    }

}
