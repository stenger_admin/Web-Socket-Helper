﻿using Core.Framework.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.ViewModel
{
    public class ScoketMessageRequest
    {
        public string msgKey { get; set; }

        public string[] msgKeys { get; set; }

        public string template { get; set; }

        public string projectToken { get; set; }

        public Pagination pagination { get; set; }
    }
}
