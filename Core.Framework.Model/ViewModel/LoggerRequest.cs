﻿using Core.Framework.Model.Common;
using System;

namespace Core.Framework.Model.ViewModel
{
    public class LoggerRequest
    {
        /// <summary>
        /// Tag标签
        /// </summary>
        public string tag { get; set; }

        /// <summary>
        /// 搜索
        /// </summary>
        public string search { get; set; }

        /// <summary>
        /// 日志主题
        /// </summary>
        public string template { get; set; }

        /// <summary>
        /// 每页显示条数
        /// </summary>
        public int rows { get; set; } = 20;

        /// <summary>
        /// 页名
        /// </summary>
        public int page { get; set; } = 1;

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime starTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime endTime { get; set; }

        /// <summary>
        /// 分页内容
        /// </summary>
        public Pagination pagination { get; set; }


        /// <summary>
        /// 兼容 Layui Table
        /// </summary>
        public void LayerTable()
        {
            this.pagination.rows = this.pagination.rows == 0 ? this.rows : this.pagination.rows;
            this.pagination.page = this.pagination.page == 0 ? this.page : this.pagination.page;
        }
    }
}
