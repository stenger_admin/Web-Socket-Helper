﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Core.Framework.Model.Common
{

    public static class PaginationExtent
    {
        /// <summary>
        /// 分页通用参数查询
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="pagination"></param>
        /// <returns></returns>
        public static List<TSource> Pagination<TSource>(this IQueryable<TSource> source, Pagination pagination)
            where TSource : class
        {

            if (pagination?.rows > 0)
            {
                pagination.records = source.Count();
                source = source.Skip(pagination.rows * (pagination.page > 0 ? pagination.page - 1 : 0));
                source = source.Take(pagination.rows);
            }

            if (!string.IsNullOrWhiteSpace(pagination?.sidx))
            {
                var param = Expression.Parameter(typeof(TSource), "t");

                var body = Expression.Property(param, pagination.sidx);

                Expression<Func<TSource, object>> keySelector = 
                    Expression.Lambda<Func<TSource, object>>(Expression.Convert(body, typeof(object)), param);

                if (null != keySelector)
                {
                    if (!string.IsNullOrWhiteSpace(pagination.sord))
                    {
                        if (pagination.sord.ToLower().Equals("asc"))
                        {
                            source = source.OrderBy(keySelector);
                        }
                        else if (pagination.sord.ToLower().Equals("desc"))
                        {
                            source = source.OrderByDescending(keySelector);
                        }
                    }
                }
            }

            return source.AsNoTracking().ToList();
        }
    }

    /// <summary>
    /// 分页实体
    /// </summary>
    public class Pagination
    {
        /// <summary>
        /// 每页行数
        /// </summary>
        public int rows { get; set; }

        /// <summary>
        /// 当前页
        /// </summary>
        public int page { get; set; }

        /// <summary>
        /// 排序列
        /// </summary>
        public string sidx { get; set; }

        /// <summary>
        /// 排序类型
        /// </summary>
        public string sord { get; set; }

        /// <summary>
        /// 总记录数
        /// </summary>
        public int records { get; set; }

        /// <summary>
        /// 总页数
        /// </summary>
        public int total
        {
            get
            {
                if (records > 0)
                {
                    return records % this.rows == 0 ? records / this.rows : records / this.rows + 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }


}
