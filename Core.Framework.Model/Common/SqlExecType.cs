﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.Model.Common
{
    /// <summary>
    /// 数据操作类型
    /// </summary>
    public enum SqlExecType
    {
        /// <summary>
        /// 添加
        /// </summary>
        insert = 1,

        /// <summary>
        /// 删除
        /// </summary>
        delete = 2,

        /// <summary>
        /// 修改
        /// </summary>
        update = 3,

        /// <summary>
        /// 查询
        /// </summary>
        select = 4


    }
}
