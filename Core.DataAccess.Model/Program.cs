using Core.DataAccess.Model.Project.Queue;
using Core.Framework.Model.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Core.DataAccess.Model
{
    public class TemporaryDbContextFactory : IDesignTimeDbContextFactory<ProjectSocketContext>
    {
        public ProjectSocketContext CreateDbContext(string[] args)
        {

            var builder = new DbContextOptionsBuilder<ProjectSocketContext>();

            builder
                .UseSqlServer("", a => a.UseRowNumberForPaging());

            return new ProjectSocketContext(builder.Options);
        }

    }
}
