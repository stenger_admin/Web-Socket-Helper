﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectClient
    {
        public int Id { get; set; }
        public int? ProjectKey { get; set; }
        public string Ios { get; set; }
        public string Android { get; set; }
        public string Pc { get; set; }
        public string Wap { get; set; }
        public string Weixin { get; set; }
        public string Db { get; set; }
    }
}
