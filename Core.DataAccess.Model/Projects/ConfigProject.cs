﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ConfigProject
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Token { get; set; }
        public int ProjectId { get; set; }
        public int? Type { get; set; }
        public string Config { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
