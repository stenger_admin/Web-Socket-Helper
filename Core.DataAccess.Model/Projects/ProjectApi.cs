﻿using System;
using System.Collections.Generic;

namespace Core.DataAccess.Model.Projects
{
    public partial class ProjectApi
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Info { get; set; }
        public DateTime? AddTime { get; set; }
    }
}
