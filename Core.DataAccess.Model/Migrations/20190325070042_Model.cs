﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.DataAccess.Model.Migrations
{
    public partial class Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Project_Module_Address",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    userId = table.Column<int>(nullable: false),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    province = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    city = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    district = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    addressName = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    info = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    tag = table.Column<string>(unicode: false, maxLength: 4, nullable: true),
                    userName = table.Column<string>(unicode: false, maxLength: 20, nullable: true),
                    userPhone = table.Column<string>(unicode: false, maxLength: 11, nullable: true),
                    def = table.Column<bool>(nullable: true, defaultValueSql: "((0))"),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_Address", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Module_Group",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    userId = table.Column<long>(nullable: false),
                    title = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    logo = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    userKeys = table.Column<string>(unicode: false, nullable: true),
                    paras = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_Group", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Module_Group_User",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    groupKey = table.Column<int>(nullable: false),
                    userId = table.Column<long>(nullable: false),
                    paras = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    type = table.Column<int>(nullable: false),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_Group_User", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Module_Params",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    userId = table.Column<long>(nullable: false),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    dic = table.Column<int>(nullable: false),
                    param = table.Column<string>(unicode: false, nullable: true),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_Params", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Module_Params_Dic",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(unicode: false, maxLength: 30, nullable: true),
                    userId = table.Column<long>(nullable: false),
                    datelist = table.Column<string>(unicode: false, maxLength: 500, nullable: true),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false, defaultValueSql: "('')"),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_Params_Dic", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Project_Module_User",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    phone = table.Column<string>(unicode: false, maxLength: 11, nullable: false),
                    userId = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    wxOpenid = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    aliPayOpenid = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    baiduOpenid = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    userName = table.Column<string>(unicode: false, maxLength: 15, nullable: true),
                    pass = table.Column<string>(unicode: false, maxLength: 32, nullable: false),
                    token = table.Column<string>(unicode: false, maxLength: 21, nullable: true, defaultValueSql: "(right(newid(),(21)))"),
                    projectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: true),
                    uuid = table.Column<string>(unicode: false, maxLength: 32, nullable: true),
                    paras = table.Column<string>(unicode: false, maxLength: 2000, nullable: true),
                    endTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    addTime = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project_Module_User", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Socket_Backups_Message",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    sendUserKey = table.Column<long>(nullable: false),
                    toUserKey = table.Column<long>(nullable: false),
                    session = table.Column<long>(nullable: false),
                    sendMessage = table.Column<string>(unicode: false, nullable: false),
                    _clientType = table.Column<int>(nullable: false),
                    _sessionType = table.Column<int>(nullable: false),
                    _messageType = table.Column<int>(nullable: false),
                    _messageContentType = table.Column<int>(nullable: false),
                    ProjectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: true),
                    sendTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Socket_Backups_Message", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Socket_Group_Message",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    sendUserKey = table.Column<long>(nullable: false),
                    toUserKey = table.Column<long>(nullable: false),
                    session = table.Column<long>(nullable: false),
                    sendMessage = table.Column<string>(unicode: false, nullable: false),
                    _clientType = table.Column<int>(nullable: false),
                    _sessionType = table.Column<int>(nullable: false),
                    _messageType = table.Column<int>(nullable: false),
                    _messageContentType = table.Column<int>(nullable: false),
                    ProjectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: true),
                    sendTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Socket_Group_Message", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Socket_Offline_Message",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    sendUserKey = table.Column<long>(nullable: false),
                    toUserKey = table.Column<long>(nullable: false),
                    session = table.Column<long>(nullable: false),
                    sendMessage = table.Column<string>(unicode: false, nullable: false),
                    _clientType = table.Column<int>(nullable: false),
                    _sessionType = table.Column<int>(nullable: false),
                    _messageType = table.Column<int>(nullable: false),
                    _messageContentType = table.Column<int>(nullable: false),
                    ProjectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: true),
                    sendTime = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Socket_Offline_Message", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Socket_User_Login",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    userKey = table.Column<long>(nullable: false),
                    ProjectToken = table.Column<string>(unicode: false, maxLength: 21, nullable: false),
                    loginTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    outTime = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Socket_User_Login", x => x.id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Project_Module_Address");

            migrationBuilder.DropTable(
                name: "Project_Module_Group");

            migrationBuilder.DropTable(
                name: "Project_Module_Group_User");

            migrationBuilder.DropTable(
                name: "Project_Module_Params");

            migrationBuilder.DropTable(
                name: "Project_Module_Params_Dic");

            migrationBuilder.DropTable(
                name: "Project_Module_User");

            migrationBuilder.DropTable(
                name: "Socket_Backups_Message");

            migrationBuilder.DropTable(
                name: "Socket_Group_Message");

            migrationBuilder.DropTable(
                name: "Socket_Offline_Message");

            migrationBuilder.DropTable(
                name: "Socket_User_Login");
        }
    }
}
