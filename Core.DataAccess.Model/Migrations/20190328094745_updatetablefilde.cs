﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.DataAccess.Model.Migrations
{
    public partial class updatetablefilde : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Template",
                table: "Socket_Offline_Message",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Template",
                table: "Socket_Group_Message",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Template",
                table: "Socket_Backups_Message",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Template",
                table: "Socket_Offline_Message");

            migrationBuilder.DropColumn(
                name: "Template",
                table: "Socket_Group_Message");

            migrationBuilder.DropColumn(
                name: "Template",
                table: "Socket_Backups_Message");
        }
    }
}
