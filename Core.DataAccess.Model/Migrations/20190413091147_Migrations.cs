﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Core.DataAccess.Model.Migrations
{
    public partial class Migrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "parameter",
                table: "ProjectUserGroupMember",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "title",
                table: "ProjectUserGroupMember",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "parameter",
                table: "ProjectUserGroupMember");

            migrationBuilder.DropColumn(
                name: "title",
                table: "ProjectUserGroupMember");
        }
    }
}
