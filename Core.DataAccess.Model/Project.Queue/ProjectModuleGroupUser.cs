﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Core.DataAccess.Model.Project.Queue
{
    [Description("关系管理 - 组员")]
    public partial class ProjectModuleGroupUser
    {
        public int Id { get; set; }
        public int GroupKey { get; set; }
        public int Level { get; set; }
        public string UserId { get; set; }
        public string Paras { get; set; }
        public int Type { get; set; }
        public string ProjectToken { get; set; }
        public DateTime? AddTime { get; set; } = DateTime.Now;
    }
}
