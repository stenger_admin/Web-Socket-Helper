﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Core.DataAccess.Model.Project.Queue
{
    [Description("群发消息")]
    public partial class SocketGroupMessage
    {
        public int Id { get; set; }

        public string MsgKey { get; set; }
        public string Template { get; set; }
        public string MsgContext { get; set; }

        public string ProjectToken { get; set; }
        public DateTime SendTime { get; set; } = DateTime.Now;
    }
}
