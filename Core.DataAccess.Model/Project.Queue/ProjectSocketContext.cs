using System;
using Core.Framework.Model.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Core.DataAccess.Model.Project.Queue
{
    /// <summary>
    /// 队列模块
    /// </summary>
    public partial class ProjectSocketContext : DbContext
    {
        public ProjectSocketContext()
        {
        }

        public ProjectSocketContext(DbContextOptions<ProjectSocketContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ProjectModuleGroup> ProjectModuleGroup { get; set; }
        public virtual DbSet<ProjectModuleGroupUser> ProjectModuleGroupUser { get; set; }
        public virtual DbSet<ProjectModuleUser> ProjectModuleUser { get; set; }
        public virtual DbSet<SocketBackupsMessage> SocketBackupsMessage { get; set; }
        public virtual DbSet<SocketGroupMessage> SocketGroupMessage { get; set; }
        public virtual DbSet<SocketOfflineMessage> SocketOfflineMessage { get; set; }
        public virtual DbSet<SocketUserLogin> SocketUserLogin { get; set; }

        #region Add Model

        public virtual DbSet<ProjectUserGroup> ProjectUserGroup { get; set; }
        public virtual DbSet<ProjectUserGroupMember> ProjectUserGroupMember { get; set; }

        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder
                    // 数据操作日志
                    .UseLoggerFactory(EFLogger.GetLoggerFactory())
                    .UseSqlServer(CoreStartupHelper.GetConnectionValue("Socket_SqlServerConnection"), a => a.UseRowNumberForPaging());
                //optionsBuilder.UseSqlServer(CoreStartupHelper.GetConnectionValue("Socket_SqlServerConnection"), a => a.UseRowNumberForPaging());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ProjectModuleGroup>(entity =>
            {
                entity.ToTable("Project_Module_Group");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Logo)
                    .HasColumnName("logo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Paras)
                    .HasColumnName("paras")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectToken)
                    .IsRequired()
                    .HasColumnName("projectToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId).HasColumnName("userId");
            });

            modelBuilder.Entity<ProjectModuleGroupUser>(entity =>
            {
                entity.ToTable("Project_Module_Group_User");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GroupKey).HasColumnName("groupKey");

                entity.Property(e => e.Paras)
                    .HasColumnName("paras")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectToken)
                    .IsRequired()
                    .HasColumnName("projectToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.Type).HasColumnName("type");

                entity.Property(e => e.UserId).HasColumnName("userId");
            });

            modelBuilder.Entity<ProjectModuleUser>(entity =>
            {
                entity.ToTable("Project_Module_User");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AddTime)
                    .HasColumnName("addTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.AliPayOpenid)
                    .HasColumnName("aliPayOpenid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BaiduOpenid)
                    .HasColumnName("baiduOpenid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndTime)
                    .HasColumnName("endTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Paras)
                    .HasColumnName("paras")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.Pass)
                    .IsRequired()
                    .HasColumnName("pass")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectToken)
                    .HasColumnName("projectToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.Token)
                    .HasColumnName("token")
                    .HasMaxLength(21)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(right(newid(),(21)))");

                entity.Property(e => e.UserId)
                    .HasColumnName("userId")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasColumnName("userName")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Uuid)
                    .HasColumnName("uuid")
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.WxOpenid)
                    .HasColumnName("wxOpenid")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SocketBackupsMessage>(entity =>
            {
                entity.ToTable("Socket_Backups_Message");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ProjectToken)
                    .HasColumnName("ProjectToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.SendTime)
                    .HasColumnName("sendTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<SocketGroupMessage>(entity =>
            {
                entity.ToTable("Socket_Group_Message");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ProjectToken)
                    .HasColumnName("ProjectToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.SendTime)
                    .HasColumnName("sendTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<SocketOfflineMessage>(entity =>
            {
                entity.ToTable("Socket_Offline_Message");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ProjectToken)
                    .HasColumnName("ProjectToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.SendTime)
                    .HasColumnName("sendTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<SocketUserLogin>(entity =>
            {
                entity.ToTable("Socket_User_Login");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.LoginTime)
                    .HasColumnName("loginTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.OutTime)
                    .HasColumnName("outTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ProjectToken)
                    .IsRequired()
                    .HasColumnName("ProjectToken")
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.UserKey).HasColumnName("userKey");
            });
        }
    }
}
