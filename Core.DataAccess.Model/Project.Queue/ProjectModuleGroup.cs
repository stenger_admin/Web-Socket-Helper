﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Core.DataAccess.Model.Project.Queue
{
    [Description("关系管理 - 组")]
    public partial class ProjectModuleGroup
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        public string ProjectToken { get; set; }
        public string Logo { get; set; }
        public string Paras { get; set; }
        public DateTime? AddTime { get; set; } = DateTime.Now;
    }
}
