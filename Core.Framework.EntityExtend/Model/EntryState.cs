﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.EntityExtend
{
    public enum EntryState
    {
        Added,
        Modified
    }
}
