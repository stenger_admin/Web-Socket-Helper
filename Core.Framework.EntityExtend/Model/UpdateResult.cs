﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Framework.EntityExtend.Model
{
    public class UpdateResult<T> : IResult<T>
    {
        public string sql { get; set; }
        public DbContext context { get; set; }
        public List<Tuple<string, object, Type>> Fields { get; set; } = new List<Tuple<string, object, Type>>();
    }
}
