﻿using Core.Framework.EntityExtend;
using Core.Framework.Model.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;

namespace Core.Framework.EntityExtend
{
    public static class DataBaseUtils
    {
        public static void ExecBySqlExecType<T>(this DbContext context, T model, SqlExecType sqlExecType, Expression<Func<T, bool>> where)
            where T : class, new()
        {
            switch (sqlExecType)
            {

                case SqlExecType.insert:
                    context.Entry(model).State = EntityState.Added;
                    break;

                case SqlExecType.update:
                    context.BulkModifiedEntry(model).Where(where);
                    break;

                case SqlExecType.delete:
                    context.BulkDeleteEntry<T>(where);
                    break;
            }
        }
    }
}
