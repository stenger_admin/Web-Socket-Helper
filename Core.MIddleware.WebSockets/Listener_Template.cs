﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Middleware.WebSockets
{
    public class Listener_Template
    {
        public string template { get; set; }
        public List<string> Subscription { get; set; }
    }
}
