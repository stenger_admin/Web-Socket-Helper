﻿using Core.Framework.Model.WebSockets;
using Core.Service.TaskHandle.Task_Model;
using System.Threading.Tasks;

namespace Core.Service.TaskHandle
{
    public interface IMQMsgListener
    {

        /// <summary>
        /// 任务方法
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        DoWork_Out DoWork(MessageQueue message);

        /// <summary>
        /// 成功触发
        /// </summary>
        void Success(DoWork_Out model);

        /// <summary>
        /// 失败触发
        /// </summary>
        void Errer(DoWork_Out model);
    }
}
