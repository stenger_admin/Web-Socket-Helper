﻿using Core.Framework.Model.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.IBusiness.IUserModule.Model
{
    /// <summary>
    /// 登陆基础参数
    /// </summary>
    public class BaseLogin
    {
        /// <summary>
        /// 过期时间
        /// </summary>
        public int hour { get; set; } = 24 * 3;

        /// <summary>
        /// WS登陆参数
        /// </summary>
        public WsParameter WsParameter { get; set; }

        /// <summary>
        /// 项目信息
        /// </summary>
        public ApiProjectInfo ApiProjectInfo { get; set; }

    }
}
