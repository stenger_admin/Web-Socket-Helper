﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.IBusiness.IUserModule.Model
{
    /// <summary>
    /// 手机号码登陆
    /// </summary>
    public class LoginByPhone : BaseLogin
    {
        /// <summary>
        /// 手机号
        /// </summary>
        public string phone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string password { get; set; }

    }
}
