﻿using Core.DataAccess.Model.Project.Queue;
using Core.Framework.Model.Common;
using Core.IBusiness.IUserModule.Model;
using System;
using System.Collections.Generic;

namespace Core.IBusiness.IUserModule
{
    /// <summary>
    /// C端用户信息
    /// </summary>
    public interface ISocketUser
    {

        #region 用户身份登陆

        /// <summary>
        /// 账户密码登陆
        /// </summary>
        /// <param name="login">登陆参数</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        MethodResult<ProjectModuleUser> Login(LoginByUser login);

        /// <summary>
        /// 根据手机号登陆和密码
        /// </summary>
        /// <param name="login">登陆参数</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        MethodResult<ProjectModuleUser> LoginByPhone(LoginByPhone login);

        /// <summary>
        /// 根据微信OPENID获取用户信息|注册
        /// </summary>
        /// <param name="login">登陆参数</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        MethodResult<ProjectModuleUser> LoginByWChat(LoginByWChat login);

        /// <summary>
        /// 根据客户端UUID登陆|注册
        /// </summary>
        /// <param name="login">登陆参数</param>
        /// <returns></returns>
        MethodResult<ProjectModuleUser> LoginByClient(LoginByUUID login);


        /// <summary>
        /// 手机验证码登录
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        MethodResult<ProjectModuleUser> LoginByClientCode(LoginByPhoneCode login);


        // 发送验证码 生成一个临时的 token
        // code phone token

        #endregion

        #region 用户信息修改

        /// <summary>
        /// 修改用户信息
        /// 查找 [用户名和密码] [OPENID]
        /// 相匹配的用户
        /// </summary>
        /// <param name="model">用户信息</param>
        /// <returns>用户临时TOKEN，和用户信息</returns>
        ProjectModuleUser UpdateUserInfo(ProjectModuleUser model);

        /// <summary>
        /// 修改用户指定属性
        /// </summary>
        /// <param name="key">用户关键字</param>
        /// <param name="userToken">用户token</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="paras">属性西悉尼</param>
        /// <returns></returns>
        ProjectModuleUser UpdateUserInfoParas(int key, string userToken, string projectToken, string paras);

        #endregion

        #region 用户身份注册

        /// <summary>
        /// 用户注册
        /// 校验 [项目是否存在] [用户名是否重复] [OPENID是否重复]
        /// </summary>
        /// <param name="model">用户信息</param>
        Tuple<bool, ProjectModuleUser, string> Reg(ProjectModuleUser model);

        #endregion

        #region 获取用户信息

        /// <summary>
        /// 根据客户端UserID获取用户信息
        /// </summary>
        /// <param name="userID">用户关键字</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="hour">缓存时间（小时）</param>
        /// <returns></returns>
        MethodResult<ProjectModuleUser> LoginByKey(int userID, string projectToken, int hour);

        /// <summary>
        /// 根据临时TOKNE获取用户信息
        /// </summary>
        /// <param name="token">用户token</param>
        /// <param name="projectToken">项目token</param>
        /// <returns>是否过期|用户信息</returns>
        MethodResult<ProjectModuleUser> GetUserInfoByToken(string token, string projectToken);



        /// <summary>
        /// 获取当前项目所有用户信息
        /// </summary>
        /// <param name="projectToken">项目token</param>
        MethodResult<List<ProjectModuleUser>> GetUserInfoAll(string projectToken);

        /// <summary>
        /// 查询用户列表
        /// </summary>
        /// <param name="startTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="search">搜索关键字</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页</param>
        /// <returns></returns>
        MethodResult<List<ProjectModuleUser>> GetUserInfoAll(DateTime startTime,DateTime endTime, string search , string projectToken,Pagination pagination);

        #endregion

        #region 用户身份注销

        /// <summary>
        /// 退出登陆
        /// </summary>
        /// <param name="token">用户token</param>
        /// <param name="projectToken">项目token</param>
        /// <returns>是否过期|用户信息</returns>
        void OutLogin(string token, string projectToken);

        #endregion
    }
}
