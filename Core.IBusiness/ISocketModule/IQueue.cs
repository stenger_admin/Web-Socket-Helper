﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Framework.Model.Common;
using Core.Framework.Model.ViewModel;
using Core.Framework.Model.WebSockets;

namespace Core.IBusiness.ISocketModule
{
    /// <summary>
    /// 消息队列
    /// </summary>
    public interface IQueue
    {

        /// <summary>
        /// 发布
        /// </summary>
        /// <param name="message">消息体</param>
        /// <returns></returns>
        MethodResult<bool> Publish(QueryMessage message);

        /// <summary>
        /// 测试专用发布接口
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        MethodResult<bool> DebugPublish(QueryMessageTest entity);


        /// <summary>
        /// 订阅
        /// </summary>
        /// <param name="token">用户Token</param>
        /// <param name="subChannel">订阅渠道</param>
        /// <returns></returns>
        MethodResult<bool> Subscribe(string token, string subChannel);


        /// <summary>
        /// 撤回
        /// </summary>
        /// <param name="token">用户Token</param>
        /// <param name="subChannel">订阅渠道</param>
        /// <returns></returns>
        MethodResult<bool> UnSubscribe(string token, string subChannel);


        /// <summary>
        /// 任务失败
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        MethodResult<bool> Fail(QueueRequest model);


        /// <summary>
        /// 消息确认
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        MethodResult<bool> Confirm(QueueRequest model);


        /// <summary>
        /// 任务已完成
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        MethodResult<bool> Finished(QueueRequest model);



    }
}
