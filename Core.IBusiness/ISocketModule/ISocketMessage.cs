﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.DataAccess.Model.Project.Queue;
using Core.Framework.Model.Common;
using StackExchange.Redis;

namespace Core.IBusiness.ISocketModule
{
    /// <summary>
    /// 消息
    /// </summary>
    public interface ISocketMessage
    {

        #region 获取离线消息

        /// <summary>
        /// 获取离线消息
        /// [not delete]
        /// </summary>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketOfflineMessage> GetOfflineMessages(string projectToken, Pagination pagination);

        /// <summary>
        /// 获取离线消息
        /// [not delete]
        /// </summary>
        /// <param name="msgKey">消息key</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketOfflineMessage> GetOfflineMessagesByMsgKey(string msgKey, string projectToken, Pagination pagination);

        /// <summary>
        /// 获取离线消息
        /// [delete]
        /// </summary>
        /// <param name="msgKeys">订阅消息集合</param>
        /// <param name="template">主题</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketOfflineMessage> GetOfflineMessagesByMsgKeyAndTemplate(string[] msgKeys, string template, string projectToken, Pagination pagination);

        #endregion

        #region 获取群组消息

        /// <summary>
        /// 获取群组消息
        /// </summary>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketGroupMessage> GetGroupMessages(string projectToken, Pagination pagination);

        /// <summary>
        /// 获取群组消息
        /// </summary>
        /// <param name="msgKey">消息key</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketGroupMessage> GetGroupMessagesByMsgKey(string msgKey, string projectToken, Pagination pagination);

        /// <summary>
        /// 获取群组消息
        /// </summary>
        /// <param name="msgKey">消息key</param>
        /// <param name="template">主题</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketGroupMessage> GetGroupMessagesByMsgKeyAndTemplate(string msgKey, string template, string projectToken, Pagination pagination);

        #endregion

        #region 获取历史消息

        /// <summary>
        /// 获取历史消息
        /// </summary>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketBackupsMessage> GetBackupsMessages(string projectToken, Pagination pagination);


        /// <summary>
        /// 获取历史消息
        /// </summary>
        /// <param name="msgKey">消息key</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketBackupsMessage> GetBackupsMessagesByMsgKey(string msgKey, string projectToken, Pagination pagination);


        /// <summary>
        /// 获取历史消息
        /// </summary>
        /// <param name="msgKey">消息key</param>
        /// <param name="template">主题</param>
        /// <param name="projectToken">项目token</param>
        /// <param name="pagination">分页信息</param>
        /// <returns></returns>
        List<SocketBackupsMessage> GetBackupsMessagesByMsgKeyAndTemplate(string msgKey, string template, string projectToken, Pagination pagination);


        #endregion

        #region 保存数据

        /// <summary>
        /// 单一订阅用户离线消息
        /// </summary>
        /// <param name="values"></param>
        void SaveSingleOffLine(RedisValue[] values);

        /// <summary>
        /// 历史消息
        /// </summary>
        /// <param name="values"></param>
        void SaveHistory(RedisValue[] values);

        #endregion

    }
}
