﻿using Core.ApiClient.Model;
using Core.Framework.Model.Common;
using Core.Framework.Model.ViewModel;
using Core.Framework.Model.WebSockets;
using Core.Framework.Util;
using Core.Middleware.WebSockets;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Core.ApiClient.Controllers.Queue
{
    /// <summary>
    /// 任务管理
    /// </summary>
    [Route(".v1/queue/")]
    [ApiController]
    public class QueueController : BaseApiController
    {
        /// <summary>
        /// 发布任务信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".push")]
        public IResult Push(QueryMessage model)
        {
            if (string.IsNullOrWhiteSpace(model.Token))
                model.Token = Guid.NewGuid().ToString();

            var result = iQueue.Publish(model);

            if (result.IsFinished)
            {
                return new ApiResult { code = CodeResult.SUCCESS, date = model.Token };
            }
            else
            {
                return new ApiResult { code = CodeResult.BUSINESS_ERROR, date = model, info = result.Discription };
            }

        }

        /// <summary>
        /// 调试专用 发布任务信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".testpush")]
        public IResult TestPush(QueryMessageTest model)
        {

            if (string.IsNullOrWhiteSpace(model.Token))
                model.Token = Guid.NewGuid().ToString();

            var result = iQueue.DebugPublish(model);

            if (result.IsFinished)
            {
                return new ApiResult { code = CodeResult.SUCCESS, date = model.Token };
            }
            else
            {
                return new ApiResult { code = CodeResult.BUSINESS_ERROR, date = model, info = result.Discription };
            }

        }

        /// <summary>
        /// 收到任务确认
        /// </summary>
        /// <returns></returns>
        [HttpPost(".Confirm")]
        public IResult Confirm(QueueRequest model)
        {
            var result = iQueue.Confirm(model);

            if (result.IsFinished)
            {
                return new ApiResult { code = CodeResult.SUCCESS, date = model };
            }
            else
            {
                return new ApiResult { code = CodeResult.BUSINESS_ERROR, date = model, info = result.Discription };
            }

        }

        /// <summary>
        /// 任务已完成
        /// </summary>
        /// <returns></returns>
        [HttpPost(".Finished")]
        public IResult Finished(QueueRequest model)
        {

            var result = iQueue.Finished(model);

            if (result.IsFinished)
            {
                return new ApiResult { code = CodeResult.SUCCESS, date = model };
            }
            else
            {
                return new ApiResult { code = CodeResult.BUSINESS_ERROR, date = model, info = result.Discription };
            }

        }

        /// <summary>
        /// 任务失败
        /// </summary>
        /// <returns></returns>
        [HttpPost(".Fail")]
        public IResult Fail(QueueRequest model)
        {

            var result = iQueue.Fail(model);

            if (result.IsFinished)
            {
                return new ApiResult { code = CodeResult.SUCCESS, date = model };
            }
            else
            {
                return new ApiResult { code = CodeResult.BUSINESS_ERROR, date = model, info = result.Discription };
            }

        }

    }
}
