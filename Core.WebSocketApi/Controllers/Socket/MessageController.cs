﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Framework.Model.Common;
using Core.Framework.Model.ViewModel;
using Core.ApiClient.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Core.ApiClient.Controllers.Socket
{
    /// <summary>
    /// 查询任务消息
    /// </summary>
    [Route(".v1/socket/[controller]")]
    [ApiController]
    public class MessageController : BaseApiController
    {
        /// <summary>
        /// 获取离线任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IResult GetOfflineMessages(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetOfflineMessages(model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND:CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 获取指定关键字离线任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".key")]
        public IResult GetOfflineMessagesByMsgKey(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetOfflineMessagesByMsgKey(model.msgKey, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 获取指定关键字集合离线任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".keyAsTemp")]
        public IResult GetOfflineMessagesByMsgKeyAsTemplate(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage
                .GetOfflineMessagesByMsgKeyAndTemplate(model.msgKeys, model.template, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 获取群组任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".groupMsg")]
        public IResult GetGroupMessages(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetGroupMessages(model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 获取指定关键字群组的任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".groupMsgBykey")]
        public IResult GetGroupMessagesByMsgKey(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetGroupMessagesByMsgKey(model.msgKey, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 获取群组指定主题的任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".groupMsgByKeyAsTemp")]
        public IResult GetGroupMessagesByMsgKeyAsTemplate(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetGroupMessagesByMsgKeyAndTemplate(model.msgKey, model.template, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 获取历史任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".backupsMsg")]
        public IResult GetBackupsMessages(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetBackupsMessages(model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 获取历史任务根据关键字
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".backupsMsgBykey")]
        public IResult GetBackupsMessagesByMsgKey(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetBackupsMessagesByMsgKey(model.msgKey, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

        /// <summary>
        /// 获取历史任务根据关键字和主题
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".backupsMsgBykeyAsTemp")]
        public IResult GetBackupsMessagesByMsgKeyAsTemplate(ScoketMessageRequest model)
        {
            var result = this.iSocketMessage.GetBackupsMessagesByMsgKeyAndTemplate(model.msgKey, model.template, model.projectToken, model.pagination);
            return new ApiResult
            {
                code = result == null ? CodeResult.DATA_NOT_FOUND : CodeResult.SUCCESS,
                date = result
            };
        }

    }
}
