﻿using Core.ApiClient.Model;
using Core.Framework.Model.WebSockets;
using Core.Framework.Redis;
using Core.Framework.Redis.Queue_Helper;
using Core.Middleware.WebSockets;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Core.ApiClient.Controllers
{

    /// <summary>
    /// 数据统计
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticsController : ControllerBase
    {

        /// <summary>
        /// 获取所有服务器列表
        /// </summary>
        /// <returns></returns>
        [HttpGet(".Service")]
        public IResult Service()
        {
            var redis = new RedisHelper();
            var keys = redis.HashKeys(RedisQueueHelperParameter.QueueService);

            List<QueueService> list = new List<QueueService>();

            // 构造服务列表
            foreach (var item in keys)
                list.Add(redis.HashGet<QueueService>(RedisQueueHelperParameter.QueueService, item));

            return new ApiResult
            {
                code = CodeResult.SUCCESS,
                date = list
            };
        }

        /// <summary>
        /// 获取当前系统所有客户机
        /// </summary>
        /// <returns></returns>
        [HttpGet(".Clients")]
        public IResult Clients()
        {
            return new ApiResult
            {
                code = CodeResult.SUCCESS,
                date = WebSocketApplication.ClientsPool.ToList()
            };
        }

        /// <summary>
        /// 当前服务器任务队列
        /// </summary>
        /// <returns></returns>
        [HttpGet(".TaskMsg")]
        public IResult TaskMsg()
        {
            var redis = new RedisHelper(4);
            return new ApiResult
            {
                code = CodeResult.SUCCESS,
                date = redis.SortedGetRangeByRank<MessageQueue>(RedisQueueHelperParameter.Queue)
            };
        }
    }
}
