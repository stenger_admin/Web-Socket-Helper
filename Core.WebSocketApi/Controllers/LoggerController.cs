﻿using Core.ApiClient.Model;
using Core.Framework.Model.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Core.ApiClient.Controllers
{
    /// <summary>
    /// 日志系统
    /// </summary>
    [Route(".v1/logs/")]
    [ApiController]
    public class LoggerController : BaseApiController
    {

        /// <summary>
        /// 获取日志的查询参数[Parameter]
        /// </summary>
        /// <returns></returns>
        [HttpPost(".parameter")]
        public IResult Template(string template = "commom")
        {
            return new ApiResult
            {
                code = CodeResult.SUCCESS,
                date = new
                {
                    tags = this.iLog.GetTags(template, iClientInfo.ProjectToken),
                    templates = this.iLog.GetTemplates(template, iClientInfo.ProjectToken),
                }
            };
        }


        /// <summary>
        /// 搜索日志
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".search")]
        public IResult Search(LoggerRequest model)
        {

            model.LayerTable();

            var result = this.iLog.GetLoggers(
                model.pagination,
                tag: model.tag,
                search: model.search,
                template: model.template,
                starTime: model.starTime,
                endTime: model.endTime,
                projectToken: iClientInfo.ProjectToken);

            return new ApiResult {
                code = CodeResult.SUCCESS,
                date = new
                {
                    pagination = model.pagination,
                    data = result
                }
            };
        }

        /// <summary>
        /// 搜索Queue日志
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".searchQueue")]
        public IResult Queue(LoggerRequest model)
        {

            model.pagination.rows = model.rows;
            model.pagination.page = model.page;

            var result = this.iLog.GetQueueLoggers(
                model.pagination,
                search: model.search,
                starTime: model.starTime,
                endTime: model.endTime,
                projectToken: iClientInfo.ProjectToken);

            return new ApiResult
            {
                code = CodeResult.SUCCESS,
                date = new
                {
                    pagination = model.pagination,
                    data = result
                }
            };
        }

        /// <summary>
        /// 添加日志
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(".add")]
        public IResult Customize(LoggerAddRequest model)
        {

            this.iLog.Customize<LoggerController>(
                tag: model.tag,
                template: model.template,
                value: model.value,
                logFlag: model.logFlag,
                projectToken: iClientInfo.ProjectToken);

            return new ApiResult { code = CodeResult.SUCCESS, info = "添加成功" };
        }

    }
}
