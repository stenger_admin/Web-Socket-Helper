﻿using Core.Middleware.WebSockets;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.ApiClient
{
    public static class ApplicationLifetime
    {
        /// <summary>
        /// 注册系统退出事件
        /// </summary>
        /// <param name="lifetime"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationLifetime RegisterApplicationLifetime(this IApplicationLifetime lifetime , IApplicationBuilder app)
        {
            lifetime.ApplicationStopped.Register(() =>
            {
                Console.WriteLine("系统退出");
            });

            lifetime.ApplicationStopping.Register(() =>
            {
                Console.WriteLine("系统准备退出...");
                app.UninstallWebSocketServiceClient();
            });

            return lifetime;
        }
    }
}
